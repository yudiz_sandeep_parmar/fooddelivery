const { newsletters } = require('../models/')
class newsletterController {
	async createNewsLetter(req, res) {
		try {
			const body = {
				iUserId: req.body.iUserId,
				sMessage: req.body.sMessage,
			}
			const insert = await newsletters.create(body)
			return res.status(200).json({ insert })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}
	async getNewsLetter(req, res) {
		try {
			const data = await newsletters.findAll()
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}
}
module.exports = new newsletterController()
