const { QueryTypes } = require('sequelize')
// const { sequelize } = require('../db/sequelize')
const { users, addresses } = require('../models')

class UserController {
	async getUserList(req, res) {
		try {
			const data = await users.findAll()
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}

	async createUser(req, res) {
		try {
			const body = {
				sFullName: req.body.sFullName,
				sEmail: req.body.sEmail,
				sPassword: req.body.sPassword,
				sMobile: req.body.sMobile,
			}
			const user = await users.create(body)
			return res.status(201).json({ user })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}
	async firstTransaction(req, res) {
		const t1 = await users.sequelize.transaction()
		try {
			const user = await users.findAll({ where: { id: 1 }, transaction: t1 })
			// const user = await users.sequelize.query('select sFullName from users where id=1', {
			// 	type: QueryTypes.SELECT,
			// 	transaction: t1,
			// })
			console.log('1st log', user[0].dataValues)

			setTimeout(async () => {
				await users.update({ sFullName: 'AAA' }, { where: { id: 1 }, transaction: t1 })
				await t1.commit()
			}, 5000)
			// const user2 = await users.sequelize.query('select * from users where id=1', { type: QueryTypes.SELECT })
			// console.log(user2)
		} catch (e) {
			await t1.rollback()
			// return res.status(500).json({ message: e.message })
		}
	}
	async secondTransaction(req, res) {
		const t2 = await users.sequelize.transaction()
		try {
			const user = await users.findAll({ where: { id: 1 }, transaction: t2 })
			console.log('2nd log', user[0].dataValues)
			setTimeout(async () => {
				// const user = await users.sequelize.query('select sFullName from users where id=1', { type: QueryTypes.SELECT })
				const user = await users.findAll({ where: { id: 1 }, transaction: t2 })
				console.log('3rd log', user[0].dataValues)
				await t2.commit()
			}, 6000)
		} catch (e) {
			await t2.rollback()
			// return res.status(500).json({ message: e.message })
		}
	}
	async repeatable_read(req, res) {
		const t1 = await users.sequelize.transaction()
		const t2 = await users.sequelize.transaction()
		try {
			console.log('........Non-Repeatable Read Solution : REPEATABLE_READ...........')

			console.log('Transaction 1 Started...')
			// await users.update({ sFullName: 'ABC' }, { where: { id: 1 }, transaction: t1 })
			const user = await users.findAll({ where: { id: 1 }, transaction: t1 })
			console.log('1st log', user[0].dataValues)
			// console.log(`sum of nQuantity from t1 : ${sum1}`)

			console.log('Transaction 2 Started...')
			await users.update({ sFullName: 'XAW' }, { where: { id: 1 }, transaction: t2 })
			const user2 = await users.findAll({ where: { id: 1 }, transaction: t2 })
			console.log('2nd log', user2[0].dataValues)
			console.log('updated in Transaction 2')

			console.log('Transaction 2 COMMIT')
			await t2.commit()

			console.log('Transaction 1 continues...')
			await users.update({ sFullName: 'SWA' }, { where: { id: 1 }, transaction: t1 })
			const user3 = await users.findAll({ where: { id: 1 }, transaction: t1 })
			console.log('3rd log', user3[0].dataValues)
			// console.log(`sum of nQuantity from t1 : ${sum2}`)

			console.log('Transaction 1 COMMIT')
			await t1.commit()
		} catch (error) {
			await t1.rollback()
			await t2.rollback()
		}
	}
}
module.exports = new UserController()
