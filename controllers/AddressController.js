const { addresses, users, newsletters } = require('../models/')

class AddressController {
	async addAddress(req, res) {
		try {
			const body = {
				iUserId: req.body.iUserId,
				sFirstName: req.body.sFirstName,
				sLastName: req.body.sLastName,
				sEmail: req.body.sEmail,
				sMobile: req.body.sMobile,
				sAddressLine1: req.body.sAddressLine1,
				sAddressLine2: req.body.sAddressLine2,
				sCity: req.body.sCity,
				sState: req.body.sState,
				sCountry: req.body.sCountry,
				eType: req.body.eType,
			}
			const insert = await addresses.create(body)
			return res.status(200).json({ insert })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}
	async getAddressList(req, res) {
		try {
			const data = await addresses.findAll()
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}
	async getUserAddress(req, res) {
		try {
			const data = await users.findAll({
				// where: { id: req.params.id },
				include: [{ model: addresses, required: true }, { model: newsletters }],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}
}

module.exports = new AddressController()
