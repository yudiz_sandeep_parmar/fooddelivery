const { Sequelize, Transaction } = require('sequelize')

const sequelize = new Sequelize('test', process.env.SQL_USER, process.env.SQL_PASSWORD, {
	host: process.env.SQL_URL,
	dialect: 'mysql',
	isolationLevel: Transaction.ISOLATION_LEVELS.REPEATABLE_READ,
})
try {
	sequelize.authenticate()
	console.log('Connection has been established successfully.')
} catch (error) {
	console.error('Unable to connect to the database:', error)
}

const db = {}
db.Sequelize = Sequelize
db.sequelize = sequelize
// db.Users = require('../models/user')(dbConnection, Sequelize)

module.exports = db
