const addresses = require('./schema/addresses')
const admin = require('./schema/admin')
const carts = require('./schema/carts')
const categories = require('./schema/categories')
const contactUs = require('./schema/contactUs')
const coupons = require('./schema/coupons')
const deliveryAgents = require('./schema/deliveryAgents')
const faq = require('./schema/faq')
const newsletters = require('./schema/newsletters')
const orders = require('./schema/orders')
const products = require('./schema/products')
const restaurants = require('./schema/restaurants')
const reviews = require('.//schema/reviews')
const shipping = require('./schema/shipping')
const transactions = require('./schema/transactions')
const users = require('./schema/users')
const userAuth = require('./schema/userAuth')
const userPaymentInfo = require('./schema/userPaymentInfo')
const wishlists = require('./schema/wishlists')

// users's relations
users.hasMany(addresses, { foreignKey: 'iUserId' })
addresses.belongsTo(users, { foreignKey: 'iUserId' })
users.hasMany(carts, { foreignKey: 'iUserId' })
carts.belongsTo(users, { foreignKey: 'iUserId' })
users.hasMany(orders, { foreignKey: 'iUserId' })
orders.belongsTo(users, { foreignKey: 'iUserId' })
users.hasMany(userAuth, { foreignKey: 'iUserId' })
userAuth.belongsTo(users, { foreignKey: 'iUserId' })
users.hasMany(userPaymentInfo, { foreignKey: 'iUserId' })
userPaymentInfo.belongsTo(users, { foreignKey: 'iUserId' })
users.hasMany(wishlists, { foreignKey: 'iUserId' })
wishlists.belongsTo(users, { foreignKey: 'iUserId' })
users.hasOne(newsletters, { foreignKey: 'iUserId' })
newsletters.belongsTo(users, { foreignKey: 'iUserId' })
users.hasMany(reviews, { foreignKey: 'iUserId' })
reviews.belongsTo(users, { foreignKey: 'iUserId' })

//category's relations
categories.hasMany(products, { foreignKey: 'iCategoryId' })
products.belongsTo(categories, { foreignKey: 'iCategoryId' })

//product's relations
products.hasMany(orders, { foreignKey: 'iProductId' })
orders.belongsTo(products, { foreignKey: 'iProductId' })
products.hasMany(wishlists, { foreignKey: 'iProductId' })
wishlists.belongsTo(products, { foreignKey: 'iProductId' })
products.hasMany(carts, { foreignKey: 'iProductId' })
carts.belongsTo(products, { foreignKey: 'iProductId' })

//orders's relations
orders.hasOne(transactions, { foreignKey: 'iOrderId' })
transactions.belongsTo(orders, { foreignKey: 'iOrderId' })
orders.hasOne(shipping, { foreignKey: 'iOrderId' })
shipping.belongsTo(orders, { foreignKey: 'iOrderId' })

//coupons's relations
coupons.hasMany(carts, { foreignKey: 'iCouponId' })
carts.belongsTo(coupons, { foreignKey: 'iCouponId' })

// restaurant's relations
restaurants.hasMany(products, { foreignKey: 'iRestaurantId' })
products.belongsTo(restaurants, { foreignKey: 'iRestaurantId' })
restaurants.hasMany(reviews, { foreignKey: 'iRestaurantId' })
reviews.belongsTo(restaurants, { foreignKey: 'iRestaurantId' })

//deliveryAgents's relations
deliveryAgents.hasMany(shipping, { foreignKey: 'iAgentId' })
shipping.belongsTo(deliveryAgents, { foreignKey: 'iAgentId' })

//'s relations

module.exports = {
	addresses,
	admin,
	carts,
	categories,
	contactUs,
	coupons,
	deliveryAgents,
	faq,
	newsletters,
	orders,
	products,
	restaurants,
	shipping,
	transactions,
	users,
	userAuth,
	userPaymentInfo,
	wishlists,
}
