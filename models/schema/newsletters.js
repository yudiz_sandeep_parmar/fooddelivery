const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Newsletter extends Model {}

Newsletter.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'users',
				key: 'id',
			},
		},
		sMessage: {
			type: DataTypes.STRING(150),
			allowNull: false,
		},
		bIsSubscribed: {
			type: DataTypes.BOOLEAN,
			defaultValue: true,
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'newsletters',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Newsletter
