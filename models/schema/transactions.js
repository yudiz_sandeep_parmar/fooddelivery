const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Transaction extends Model {}

Transaction.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iOrderId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'orders',
				key: 'id',
			},
		},
		eStatus: {
			type: DataTypes.ENUM,
			values: ['Refunded', 'Succeeded', 'Failed', 'Cancelled'],
			allowNull: false,
		},
		ePaymentMethod: {
			type: DataTypes.ENUM,
			values: ['Card', 'COD', 'Net Banking', 'Wallet'],
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'transactions',
		indexes: [
			{
				name: 'transactions_e_status',
				fields: ['eStatus'],
			},
		],
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Transaction
