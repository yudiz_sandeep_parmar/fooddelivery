const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class ContactUs extends Model {}

ContactUs.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		sFullName: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		sEmail: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		bAcknowledge: {
			type: DataTypes.BOOLEAN,
			defaultValue: false,
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'contactUs',
		indexes: [
			{
				name: 'contact_us_s_email_s_full_name',
				fields: ['sEmail', 'sFullName'],
				where: {
					bIsActive: true,
				},
			},
		],
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = ContactUs
