const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class User extends Model {}

User.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		sRestaurantName: {
			type: DataTypes.STRING(50),
			allowNull: false,
		},
		sEmail: {
			type: DataTypes.STRING(25),
			allowNull: false,
		},
		sPassword: {
			type: DataTypes.STRING(50),
			allowNull: false,
		},
		sCity: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		sState: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		nPinCode: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		bIsActive: {
			type: DataTypes.BOOLEAN,
			defaultValue: true,

			allowNull: false,
		},
		sMobile: {
			type: DataTypes.STRING(15),
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'restaurants',
		indexes: [
			{
				name: 'restaurants_s_restaurant_name',
				fields: ['sRestaurantName'],
				where: {
					bIsActive: true,
				},
			},
		],

		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = User
