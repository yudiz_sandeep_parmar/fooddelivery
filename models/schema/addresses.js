const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Address extends Model {}

Address.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'users',
				key: 'id',
			},
		},
		sFirstName: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		sLastName: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		sEmail: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		sMobile: {
			type: DataTypes.STRING(15),
			allowNull: false,
		},
		sAddressLine1: { type: DataTypes.STRING(80), allowNull: false },
		sAddressLine2: { type: DataTypes.STRING(80), allowNull: false },
		sCity: { type: DataTypes.STRING(25), allowNull: false },
		sState: { type: DataTypes.STRING(25), allowNull: false },
		sCountry: { type: DataTypes.STRING(40), allowNull: false },
		eType: {
			type: DataTypes.ENUM('Home', 'Office', 'Others'),
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'addresses',
		indexes: [
			{
				name: 'address_s_email_s_mobile',
				fields: ['sEmail', 'sMobile'],
			},
		],
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)
module.exports = Address
