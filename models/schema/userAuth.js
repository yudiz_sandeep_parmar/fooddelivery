const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class UserAuth extends Model {}

UserAuth.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'users',
				key: 'id',
			},
		},
		sToken: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'userAuth',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = UserAuth
