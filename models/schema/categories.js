const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Category extends Model {}

Category.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		sCategoryName: {
			type: DataTypes.STRING(25),
			allowNull: false,
			unique: true,
		},
	},
	{
		sequelize: db.sequelize,

		modelName: 'categories',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Category
