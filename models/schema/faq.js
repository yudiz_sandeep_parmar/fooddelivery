const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class FAQ extends Model {}

FAQ.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		sQuestion: {
			type: DataTypes.STRING(50),
			allowNull: false,
		},
		sAnswer: {
			type: DataTypes.STRING(50),
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'faq',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = FAQ
