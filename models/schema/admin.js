const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Admin extends Model {}

Admin.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		sFullName: {
			type: DataTypes.STRING(25),
			allowNull: false,
		},
		sEmail: {
			type: DataTypes.STRING(25),
			allowNull: false,
			unique: true,
		},
		sPassword: {
			type: DataTypes.STRING(50),
			allowNull: false,
		},
		bIsActive: {
			type: DataTypes.BOOLEAN,
			defaultValue: true,
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,

		modelName: 'admin',
		indexes: [
			{
				name: 'admin_s_full_name',
				fields: ['sFullName'],
			},
		],
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Admin
