const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')
class Wishlist extends Model {}

Wishlist.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'users',
				key: 'id',
			},
		},
		iProductId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'products',
				key: 'id',
			},
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'wishlists',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Wishlist
