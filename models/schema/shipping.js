const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Shipping extends Model {}

Shipping.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iOrderId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'orders',
				key: 'id',
			},
		},
		eStatus: {
			type: DataTypes.ENUM,
			values: ['Created', 'Shipped', 'Delivered', 'Failed', 'Cancelled'],
			allowNull: false,
		},
		iAgentId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'deliveryAgent',
				key: 'id',
			},
		},
		dDeliveryTime: {
			allowNull: false,
			type: DataTypes.DATE,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'shipping',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Shipping
