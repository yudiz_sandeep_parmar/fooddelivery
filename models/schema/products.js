const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Product extends Model {}

Product.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iCategoryId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'categories',
				key: 'id',
			},
			onDelete: 'cascade',
		},
		sProductName: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		nPrice: {
			type: DataTypes.DECIMAL(10, 2),
			allowNull: false,
		},
		nAmount: {
			type: DataTypes.DECIMAL(10, 2),
			allowNull: false,
		},
		sDesc: {
			type: DataTypes.STRING(100),
			allowNull: false,
		},
		bIsActive: {
			type: DataTypes.BOOLEAN,
			defaultValue: true,
			allowNull: false,
		},
		iRestaurantId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'restaurants',
				key: 'id',
			},
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'products',
		indexes: [
			{
				name: 'products_s_product_name',
				fields: ['sProductName'],
				where: {
					bIsActive: true,
				},
			},
		],
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Product
