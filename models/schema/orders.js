const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Order extends Model {}

Order.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'users',
				key: 'id',
			},
		},
		iProductId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'products',
				key: 'id',
			},
		},
		nQuantity: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		nAmount: {
			type: DataTypes.DECIMAL(10, 2),
			allowNull: false,
		},
		nDeliveryCharge: {
			type: DataTypes.DECIMAL(10, 2),
			allowNull: false,
		},
		nFinalAmount: {
			type: DataTypes.DECIMAL(10, 2),
			allowNull: false,
		},
		nDiscount: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		dActivityDate: {
			type: DataTypes.DATE,
			allowNull: false,
		},
		sOrderNote: {
			type: DataTypes.STRING(30),
			allowNull: false,
		},
		nTip: {
			type: DataTypes.INTEGER,
			defaultValue: 0,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'orders',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Order
