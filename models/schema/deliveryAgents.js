const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class DeliveryAgent extends Model {}

DeliveryAgent.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		sFullName: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		sEmail: {
			type: DataTypes.STRING,
			allowNull: false,
			unique: true,
		},
		sPassword: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		sMobile: {
			type: DataTypes.STRING(15),
			allowNull: false,
		},
		bIsActive: {
			type: DataTypes.BOOLEAN,
			defaultValue: true,
			allowNull: false,
		},
		sCity: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'deliveryAgents',
		indexes: [
			{
				name: 'deliveryAgent_s_email_s_full_name',
				fields: ['sEmail', 'sFullName'],
				where: {
					bIsActive: true,
				},
			},
		],
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = DeliveryAgent
