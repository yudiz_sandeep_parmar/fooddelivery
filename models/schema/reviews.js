const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Review extends Model {}

Review.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iRestaurantId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'restaurants',
				key: 'id',
			},
		},
		iUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'users',
				key: 'id',
			},
		},
		eReview: {
			type: DataTypes.ENUM,
			values: ['1', '2', '3', '4', '5'],
			allowNull: false,
			defaultValue: '5',
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'reviews',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Review
