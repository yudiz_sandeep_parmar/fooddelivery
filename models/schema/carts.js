const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Cart extends Model {}

Cart.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'users',
				key: 'id',
			},
		},
		iProductId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'products',
				key: 'id',
			},
		},
		nQuantity: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		nAmount: {
			type: DataTypes.DECIMAL(10, 2),
			allowNull: false,
		},
		iCouponId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'coupons',
				key: 'id',
			},
		},
	},
	{
		sequelize: db.sequelize,

		modelName: 'carts',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Cart
