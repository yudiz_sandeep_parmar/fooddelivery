const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class User extends Model {}

User.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		sFullName: {
			type: DataTypes.STRING(25),
			allowNull: false,
		},
		sEmail: {
			type: DataTypes.STRING(25),
			allowNull: false,
			unique: true,
		},
		sPassword: {
			type: DataTypes.STRING(50),
			allowNull: false,
		},
		sMobile: {
			type: DataTypes.STRING(15),
			allowNull: false,
			unique: true,
		},
		bIsActive: {
			type: DataTypes.BOOLEAN,
			defaultValue: true,
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'users',
		indexes: [
			{
				name: 'users_s_full_name',
				fields: ['sFullName'],
				where: {
					bIsActive: true,
				},
			},
		],
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = User
