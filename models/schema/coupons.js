const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class Coupon extends Model {}

Coupon.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		sCouponCode: {
			type: DataTypes.STRING(10),
			allowNull: false,
			unique: true,
		},
		nDiscount: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		dExpiryDate: {
			type: DataTypes.DATE,
			allowNull: false,
		},
		nUsageUser: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		nUsage: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'coupons',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = Coupon
