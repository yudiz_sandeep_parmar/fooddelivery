const { DataTypes, Model } = require('sequelize')
const db = require('../../db/sequelize')

class UserPaymentInfo extends Model {}

UserPaymentInfo.init(
	{
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.BIGINT,
		},
		iUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'users',
				key: 'id',
			},
		},
		nCardNumber: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
	},
	{
		sequelize: db.sequelize,
		modelName: 'user_payment_info',
		timestamps: true,
		createdAt: 'dCreatedAt',
		updatedAt: 'dUpdatedAt',
	}
)

module.exports = UserPaymentInfo
