'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('shipping', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			iOrderId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'orders',
					key: 'id',
				},
			},
			eStatus: {
				type: DataTypes.ENUM,
				values: ['Created', 'Shipped', 'Delivered', 'Failed', 'Cancelled'],
				allowNull: false,
			},
			iAgentId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'deliveryAgent',
					key: 'id',
				},
			},
			dDeliveryTime: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
	},

	async down(queryInterface) {
		await queryInterface.dropTable('shipping')
	},
}
