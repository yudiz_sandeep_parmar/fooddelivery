'use strict'
const { DataTypes } = require('sequelize')

module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('products', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			iCategoryId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'categories',
					key: 'id',
				},
				onDelete: 'cascade',
			},
			sProductName: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			nPrice: {
				type: DataTypes.DECIMAL(10, 2),
				allowNull: false,
			},
			nAmount: {
				type: DataTypes.DECIMAL(10, 2),
				allowNull: false,
			},
			sDesc: {
				type: DataTypes.STRING(100),
				allowNull: false,
			},
			bIsActive: {
				type: DataTypes.BOOLEAN,
				defaultValue: true,
				allowNull: false,
			},
			iRestaurantId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'restaurants',
					key: 'id',
				},
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
		await queryInterface.addIndex({ tableName: 'products' }, ['sProductName'], {
			where: {
				bIsActive: true,
			},
		})
	},

	async down(queryInterface) {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable('products')
	},
}
