'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('deliveryAgent', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			sFullName: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			sEmail: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true,
			},
			sPassword: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			sMobile: {
				type: DataTypes.STRING(15),
				allowNull: false,
			},
			bIsActive: {
				type: DataTypes.BOOLEAN,
				defaultValue: true,
				allowNull: false,
			},
			sCity: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
		await queryInterface.addIndex({ tableName: 'deliveryAgent' }, ['sEmail', 'sFullName'], {
			where: {
				bIsActive: true,
			},
		})
	},

	async down(queryInterface) {
		await queryInterface.dropTable('deliveryAgent')
	},
}
