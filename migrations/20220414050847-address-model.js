'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('addresses', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			iUserId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'users',
					key: 'id',
				},
			},
			sFirstName: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			sLastName: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			sEmail: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			sMobile: {
				type: DataTypes.STRING(15),
				allowNull: false,
			},
			sAddressLine1: { type: DataTypes.STRING(80), allowNull: false },
			sAddressLine2: { type: DataTypes.STRING(80), allowNull: false },
			sCity: { type: DataTypes.STRING(25), allowNull: false },
			sState: { type: DataTypes.STRING(25), allowNull: false },
			sCountry: { type: DataTypes.STRING(40), allowNull: false },
			eType: {
				type: DataTypes.ENUM('Home', 'Office', 'Others'),
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
		await queryInterface.addIndex({ tableName: 'addresses' }, ['sEmail', 'sMobile'])
	},

	async down(queryInterface) {
		await queryInterface.dropTable('addresses')
	},
}
