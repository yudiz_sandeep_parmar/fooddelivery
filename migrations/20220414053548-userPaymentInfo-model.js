'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('userPaymentInfo', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			iUserId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'users',
					key: 'id',
				},
			},
			nCardNumber: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
	},

	async down(queryInterface) {
		await queryInterface.dropTable('userPaymentInfo')
	},
}
