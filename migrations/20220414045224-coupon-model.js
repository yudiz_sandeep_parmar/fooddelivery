'use strict'
const { DataTypes } = require('sequelize')

module.exports = {
	async up(queryInterface) {
		/**
		 * Add altering commands here.
		 *
		 * Example:
		 * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
		 */
		await queryInterface.createTable('coupons', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			sCouponCode: {
				type: DataTypes.STRING(10),
				allowNull: false,
				unique: true,
			},
			nDiscount: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			dExpiryDate: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			nUsageUser: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			nUsage: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
	},

	async down(queryInterface) {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */

		await queryInterface.dropTable('coupons')
	},
}
