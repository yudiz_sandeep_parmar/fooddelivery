'use strict'
const { DataTypes } = require('sequelize')

module.exports = {
	async up(queryInterface, Sequelize) {
		/**
		 * Add altering commands here.
		 *
		 * Example:
		 * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
		 */
		await queryInterface.createTable('admin', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			sFullName: {
				type: DataTypes.STRING(25),
				allowNull: false,
			},
			sEmail: {
				type: DataTypes.STRING(25),
				allowNull: false,
				unique: true,
			},
			sPassword: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			bIsActive: {
				type: DataTypes.BOOLEAN,
				defaultValue: true,
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
		await queryInterface.addIndex({ tableName: 'admin' }, ['sFullName'], {
			where: {
				bIsActive: true,
			},
		})
	},

	async down(queryInterface) {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable('admin')
	},
}
