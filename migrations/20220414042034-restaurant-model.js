'use strict'
const { DataTypes } = require('sequelize')

module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('restaurants', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			sRestaurantName: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			sEmail: {
				type: DataTypes.STRING(25),
				allowNull: false,
			},
			sPassword: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			sCity: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			sState: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			nPinCode: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			bIsActive: {
				type: DataTypes.BOOLEAN,
				defaultValue: true,
				allowNull: false,
			},
			sMobile: {
				type: DataTypes.STRING(15),
				allowNull: false,
			},

			dCreatedAt: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
		await queryInterface.addIndex({ tableName: 'restaurants' }, ['sRestaurantName'], {
			where: {
				bIsActive: true,
			},
		})
	},

	async down(queryInterface) {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable('restaurants')
	},
}
