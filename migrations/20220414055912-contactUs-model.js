'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('contactUs', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			sFullName: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			sEmail: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			bAcknowledge: {
				type: DataTypes.BOOLEAN,
				defaultValue: false,
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
		await queryInterface.addIndex({ tableName: 'contactUs' }, ['sEmail', 'sFullName'], {
			where: {
				bIsActive: true,
			},
		})
	},

	async down(queryInterface) {
		await queryInterface.dropTable('contactUs')
	},
}
