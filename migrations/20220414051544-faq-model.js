'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('faq', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			sQuestion: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			sAnswer: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
	},

	async down(queryInterface) {
		await queryInterface.dropTable('faq')
	},
}
