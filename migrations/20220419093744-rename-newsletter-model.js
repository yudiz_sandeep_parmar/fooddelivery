'use strict'

module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.renameTable('newsletter', 'newsletters')
	},

	async down(queryInterface, Sequelize) {
		await queryInterface.renameTable('newsletters', 'newsletter')
	},
}
