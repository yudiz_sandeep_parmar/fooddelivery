'use strict'
const { DataTypes } = require('sequelize')

module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('reviews', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			iRestaurantId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'restaurants',
					key: 'id',
				},
			},
			iUserId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'users',
					key: 'id',
				},
			},
			eReview: {
				type: DataTypes.ENUM,
				values: ['1', '2', '3', '4', '5'],
				allowNull: false,
				defaultValue: '5',
			},
			dCreatedAt: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
	},

	async down(queryInterface) {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable('reviews')
	},
}
