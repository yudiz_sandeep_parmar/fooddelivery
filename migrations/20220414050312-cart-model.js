'use strict'
const { DataTypes } = require('sequelize')

module.exports = {
	async up(queryInterface) {
		/**
		 * Add altering commands here.
		 *
		 * Example:
		 * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
		 */
		await queryInterface.createTable('carts', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			iUserId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'users',
					key: 'id',
				},
			},
			iProductId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'products',
					key: 'id',
				},
			},
			nQuantity: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			nAmount: {
				type: DataTypes.DECIMAL(10, 2),
				allowNull: false,
			},
			iCouponId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'coupons',
					key: 'id',
				},
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
	},

	async down(queryInterface) {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */

		await queryInterface.dropTable('carts')
	},
}
