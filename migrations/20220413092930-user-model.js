'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('users', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			sFullName: {
				type: DataTypes.STRING(25),
				allowNull: false,
			},
			sEmail: {
				type: DataTypes.STRING(25),
				allowNull: false,
				unique: true,
			},
			sPassword: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			sMobile: {
				type: DataTypes.STRING(15),
				allowNull: false,
				unique: true,
			},
			bIsActive: {
				type: DataTypes.BOOLEAN,
				defaultValue: true,
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
		await queryInterface.addIndex({ tableName: 'users' }, ['sFullName'], {
			where: {
				bIsActive: true,
			},
		})
	},

	async down(queryInterface) {
		await queryInterface.dropTable('users')
	},
}
