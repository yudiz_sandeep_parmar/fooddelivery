'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('orders', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			iUserId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'users',
					key: 'id',
				},
			},
			iProductId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'products',
					key: 'id',
				},
			},
			nQuantity: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			nAmount: {
				type: DataTypes.DECIMAL(10, 2),
				allowNull: false,
			},
			nDeliveryCharge: {
				type: DataTypes.DECIMAL(10, 2),
				allowNull: false,
			},
			nFinalAmount: {
				type: DataTypes.DECIMAL(10, 2),
				allowNull: false,
			},
			nDiscount: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			dActivityDate: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			sOrderNote: {
				type: DataTypes.STRING(30),
				allowNull: false,
			},
			nTip: {
				type: DataTypes.INTEGER,
				defaultValue: 0,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
	},

	async down(queryInterface) {
		await queryInterface.dropTable('orders')
	},
}
