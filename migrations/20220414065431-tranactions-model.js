'use strict'
const { DataTypes } = require('sequelize')
module.exports = {
	async up(queryInterface) {
		await queryInterface.createTable('transactions', {
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.BIGINT,
			},
			iOrderId: {
				type: DataTypes.BIGINT,
				allowNull: false,
				references: {
					model: 'orders',
					key: 'id',
				},
			},
			eStatus: {
				type: DataTypes.ENUM,
				values: ['Refunded', 'Succeeded', 'Failed', 'Cancelled'],
				allowNull: false,
			},
			ePaymentMethod: {
				type: DataTypes.ENUM,
				values: ['Card', 'COD', 'Net Banking', 'Wallet'],
				allowNull: false,
			},
			dCreatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			dUpdatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
		})
		await queryInterface.addIndex({ tableName: 'transactions' }, ['eStatus'])
	},

	async down(queryInterface) {
		await queryInterface.dropTable('transactions')
	},
}
