require('../env')
const cors = require('cors')
const express = require('express')
const bodyParser = require('body-parser')
const router = require('../routers/router')
require('../db/sequelize')

const app = express()

app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors({ origin: '*', exposedHeaders: ['Content-Range'] }))
app.use(router)
app.listen(process.env.PORT || 3000, () => {
	console.log('Server started at port: ' + 3000)
})
