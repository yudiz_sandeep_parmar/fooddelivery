// const express = require('express')
const router = require('express').Router()

const UserController = require('../controllers/UserController')

router.post('/users/reg', UserController.createUser)
router.get('/t1', UserController.firstTransaction)
router.get('/t2', UserController.secondTransaction)
router.get('/users/list', UserController.getUserList)
router.get('/t3', UserController.repeatable_read)

module.exports = router
