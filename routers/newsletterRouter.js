const router = require('express').Router()
const newsletterController = require('../controllers/newsletterController')

router.post('/newsletter/add', newsletterController.createNewsLetter)

module.exports = router
