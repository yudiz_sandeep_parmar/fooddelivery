const router = require('express').Router()
const AddressController = require('../controllers/AddressController')

router.post('/address/add', AddressController.addAddress)
router.get('/address/', AddressController.getAddressList)
router.get('/address/user/:id', AddressController.getUserAddress)
module.exports = router
